package com.morad.gesprojet.DAO.entity;

import java.util.List;

public interface ProjetDAO {
	
	public void add(Projet e);
	
	public Projet edite(Projet e);
	
	public void  delete(Long id);
	
	public List<Projet>  findAll();
	
	public Projet findById(Long id);
	
	
	

}
