package com.morad.gesprojet.DAO.entity;

import java.util.List;

import org.hibernate.Session;

import com.morad.gesprojet.utils.HibernateUtil;

public class ProjetDAOimp implements ProjetDAO {
	
	
	 Session session = HibernateUtil.sessionFactory.openSession();

	@Override
	public void add(Projet e) {
		
		session.beginTransaction();
		session.save(e);
		session.getTransaction().commit();
		
		
	}

	@Override
	public Projet edite(Projet e) {
		
		session.beginTransaction();
		Projet p = (Projet) session.merge(e);
		session.getTransaction().commit();
		
		return p;
	
	
	}

	@Override
	public void delete (Long id) {
		session.beginTransaction();
		Projet p = findById(id);
		session.delete(p);
		session.getTransaction().commit();
		
		
	}

	@Override
	public List<Projet> findAll() {
	
		return session.createQuery("select o from Projet o").list();
	}

	@Override
	public Projet findById(Long Id) {
	
		return session.get(Projet.class, Id);
	}

}
