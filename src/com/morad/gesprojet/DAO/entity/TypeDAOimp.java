package com.morad.gesprojet.DAO.entity;

import java.util.List;

import org.hibernate.Session;

import com.morad.gesprojet.utils.HibernateUtil;

public class TypeDAOimp implements TypeDAO {
	
	
	 Session session = HibernateUtil.sessionFactory.openSession();

	@Override
	public void add(Type e) {
		
		session.beginTransaction();
		session.save(e);
		session.getTransaction().commit();
		
		
	}

	@Override
	public Type edite(Type e) {
		
		session.beginTransaction();
		Type p = (Type) session.merge(e);
		session.getTransaction().commit();
		
		return p;
	
	
	}

	@Override
	public void delete (Long id) {
		session.beginTransaction();
		Type p = findById(id);
		session.delete(p);
		session.getTransaction().commit();
		
		
	}

	@Override
	public List<Type> findAll() {
	
		return session.createQuery("select o from Projet o").list();
	}

	@Override
	public Type findById(Long Id) {
	
		return session.get(Type.class, Id);
	}

}
