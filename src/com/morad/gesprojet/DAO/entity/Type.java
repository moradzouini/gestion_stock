package com.morad.gesprojet.DAO.entity;



import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Type implements Serializable  {
	private static final long serialVersionUID = 1L;
	// d�claration des attributs
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int Id ;
	@Column(name="nom_Type")
	private String name;
	private String description;
	private String active;
	
	// constructor par d�faut
	public Type() {
		super();	
	}

	// constructor with argument
	public Type(String name, String description, String active) {
		super();
		this.name = name;
		this.description = description;
		this.active = active;
	}
	
	// G�n�ration des getters and Setters

	public int getId() {
		return Id;
	}

	public void setId(int id) {
		Id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getActive() {
		return active;
	}

	public void setActive(String active) {
		this.active = active;
	} 


	
	
	
}



