package com.morad.gesprojet.DAO.entity;

import java.util.List;

public interface TypeDAO {
	
	public void add(Type e);
	
	public Type edite(Type e);
	
	public void  delete(Long id);
	
	public List<Type>  findAll();
	
	public Type findById(Long id);
	
	
	

}
