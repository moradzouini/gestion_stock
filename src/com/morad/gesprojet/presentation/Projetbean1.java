package com.morad.gesprojet.presentation;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

import org.jboss.logging.Logger;

@ManagedBean
@RequestScoped
public class Projetbean1 {
	public Logger log = Logger.getLogger(Projetbean1.class);
	
	
private String title;
private String description;



public String getTitle() {
	return title;
}


public void setTitle(String title) {
	this.title = title;
}


public String getDescription() {
	return description;
}



public void setDescription(String description) {
	this.description = description;
}



}
