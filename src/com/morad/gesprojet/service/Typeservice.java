package com.morad.gesprojet.service;

import java.util.List;

import com.morad.gesprojet.DAO.entity.Type;

public interface Typeservice {
	
	public void add(Type e);
	
	public Type edite(Type e);
	
	public void  delete(Long id);
	
	public List<Type>  findAll();
	
	public Type findById(Long id);
	
	
	

}