package com.morad.gesprojet.service;

import java.util.List;

import com.morad.gesprojet.DAO.entity.Projet;

public interface Projetservice {
	

	public void add(Projet e);
	
	public Projet edite(Projet e);
	
	public void  delete(Long id);
	
	public List<Projet>  findAll();
	
	public Projet findById(Long id);
	
	
	

}
