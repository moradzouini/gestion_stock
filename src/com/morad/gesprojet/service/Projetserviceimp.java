package com.morad.gesprojet.service;

import java.util.List;

import com.morad.gesprojet.DAO.entity.Projet;
import com.morad.gesprojet.DAO.entity.ProjetDAO;
import com.morad.gesprojet.DAO.entity.ProjetDAOimp;

public class Projetserviceimp implements Projetservice {
	
	ProjetDAO dao = new ProjetDAOimp();

	@Override
	public void add(Projet e) {
		dao.add(e);
		
	}

	@Override
	public Projet edite(Projet e) {
		return dao.edite(e);
	}

	@Override
	public void delete(Long id) {
		 dao.delete(id);
		
	}

	@Override
	public List<Projet> findAll() {
		return dao.findAll();
	}

	@Override
	public Projet findById(Long id) {
		// TODO Auto-generated method stub
		return null;
	}

}
